-------------------------------------------------------------------------------
--
-- Title       : RS-Trigger
-- Design      : RS_TRiGG
-- Author      : MG
-- Company     : 1
--
-------------------------------------------------------------------------------
--
-- File        : c:\My_Designs\RS_Trigg\RS_TRiGG\compile\RS-Trigger.vhd
-- Generated   : Sat Mar 31 17:03:43 2018
-- From        : c:\My_Designs\RS_Trigg\RS_TRiGG\src\RS-Trigger.bde
-- By          : Bde2Vhdl ver. 2.6
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------
-- Design unit header --
library IEEE;
use IEEE.std_logic_1164.all;


entity rs is
  port(
       R : in STD_LOGIC;
       S : in STD_LOGIC;
       Q : inout STD_LOGIC;
       notQ : inout STD_LOGIC
  );
end rs;

architecture rs_arch of rs is

begin

----  Component instantiations  ----

Q <= not(notQ or R);
                                   
notQ <= not(Q or S);


end rs_arch;
