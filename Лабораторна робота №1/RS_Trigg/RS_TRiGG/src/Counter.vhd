library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.std_logic_unsigned.all;
entity Counter is
  port(
   CLK : in STD_LOGIC;
   RST : in STD_LOGIC;
   Q : out STD_LOGIC_VECTOR(3 downto 0)
      );
end Counter;
--}} End of automatically maintained section

architecture Counter of Counter is  
signal tmp: std_logic_vector(3 downto 0);
begin  
  process (CLK, RST) 
             begin 
                   if (RST='1') then   
                          tmp <= "0000";         
                   elsif CLK='1'
        then        
                          tmp <= tmp + "0001";        
                   end if;             
             end process;       
   Q <= tmp;
end Counter;