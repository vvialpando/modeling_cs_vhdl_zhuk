-------------------------------------------------------------------------------
--
-- Title       : TFDC
-- Design      : lab2
-- Author      : MG
-- Company     : 1
--
-------------------------------------------------------------------------------
--
-- File        : TFDC.vhd
-- Generated   : Sun Apr  1 22:42:26 2018
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {TFDC} architecture {TFDC}}

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_vector_unsigned.all;

entity decoder is
  port(
  X : in STD_LOGIC_VECTOR(3 downto 0);
  Q : out STD_LOGIC_VECTOR(6 downto 0)
       );
end decoder;

architecture decoder of decoder is
begin		
	process(x) 
	begin
		case(x)	 is
			when x"0" => Y <= "1110111";   
			when x"1" => Y <= "0100100";
			when x"2" => Y <= "1101011";
			when x"3" => Y <= "1101101";
			when x"4" => Y <= "0101110";
			when x"5" => Y <= "1101011";
			when x"6" => Y <= "1111011";
			when x"7" => Y <= "0100101";
			when x"8" => Y <= "1111111";
			when x"9" => Y <= "1101111";
			when others => Y <= "00000000";
	end case;
  		   end process;
end decoder;
