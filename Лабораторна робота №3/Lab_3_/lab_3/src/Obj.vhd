-------------------------------------------------------------------------------
--
-- Title       : \111\
-- Design      : lab_3
-- Author      : MG
-- Company     : 1
--
-------------------------------------------------------------------------------
--
-- File        : Obj.vhd
-- Generated   : Tue Apr 17 00:02:02 2018
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {\111\} architecture {\111\}}

library IEEE;
use IEEE.std_logic_1164.all;

entity \111\ is
  port(
       X : in STD_LOGIC_VECTOR (2 downto 0);
       Y : out STD_LOGIC
  );
end \111\;
architecture \111\ of \111\ is
---- Signal declarations used on the diagram ----
signal A : STD_LOGIC;
signal B : STD_LOGIC;
signal C : STD_LOGIC;
signal D : STD_LOGIC;
signal E : STD_LOGIC;
signal F : STD_LOGIC;
begin
----  Component instantiations  ----
A <= not(X(0)) after 5ns;
B <= X(1) or A after 10ns;
C <= not(X(2) or A)after 10ns;
D <= C and B after 10ns;
E <= not(X(2) xor C)after 10ns;
F <= D xor B after 10ns;
Y <= not(E and F) after 10ns;
end \111\;

