-------------------------------------------------------------------------------
--
-- Title       : object
-- Design      : lab4
-- Author      : MG
-- Company     : 1
--
-------------------------------------------------------------------------------
--
-- File        : object.vhd
-- Generated   : Tue Apr 17 00:58:20 2018
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {object} architecture {object}}

			   library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity object is
	 port(
		 Bout : out STD_LOGIC;
		 Ainout : inout STD_LOGIC
	     );
end object;

--}} End of automatically maintained section

architecture object of object is  
signal CLK : std_logic :='0';
begin				   
	Pr_CLK: process
	
	begin		
		wait for 10 ns;
		CLK <= not(CLK);
	end process Pr_CLK;
	
	Pr_A: process	 
	begin	 
		wait on CLK;
		if  CLK ='1' then Ainout<='1' after 5 ns;
		elsif  CLK='0' then Ainout<='0' after 5 ns;
		end if;
		end process Pr_A;
	Pr_B: process	 
	begin	 
		wait until Ainout'event;
		Bout<= not Ainout;
	end process Pr_B;
end object;	 