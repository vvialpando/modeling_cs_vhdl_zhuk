-------------------------------------------------------------------------------
--
-- Title       : cod_5
-- Design      : laab_5
-- Author      : MG
-- Company     : 1
--
-------------------------------------------------------------------------------
--
-- File        : cod_5.vhd
-- Generated   : Tue Apr 17 00:28:42 2018
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {cod_5} architecture {cod_5}}

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity rx is
port(
	 CLK : in STD_LOGIC;
	 RE : in STD_LOGIC;
	WE : in STD_LOGIC;
	DATA_IN : in STD_LOGIC_VECTOR(7 downto 0);
	 DATA_OUT : out STD_LOGIC_VECTOR(7 downto 0)
     );
end rx;

--}} End of automatically maintained section

architecture rx of rx is
begin
process (CLK)
    variable DATA: STD_LOGIC_VECTOR(7 downto 0); 	   
    begin
        if CLK'event and CLK = '1' then
            if WE = '1' and RE = '0' then	
                DATA := DATA_IN;	
            elsif WE = '0' and RE = '1' then
                DATA_OUT <= DATA;
            else
                DATA_OUT <= "ZZZZZZZZ";
            end if;
        end if;
    end process;
end rx;

