library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity shift_rx is
 port(
	 DATA_IN : in STD_LOGIC;
	 CLK : in STD_LOGIC;
	 RE : in STD_LOGIC;
	 WE : in STD_LOGIC;
	 DATA_OUT : out STD_LOGIC_VECTOR(7 downto 0)
     );
end shift_rx;

--}} End of automatically maintained section

architecture shift_rx of shift_rx is
begin
 process (CLK)
    variable DATA: STD_LOGIC_VECTOR(7 downto 0);
    begin
        if CLK'event and CLK = '1' then
            if WE = '1' and RE = '0' then	
                for i in 7 downto 1 loop
                    DATA(i) := DATA(i - 1);	
                end loop;
                DATA(0) := DATA_IN;
            elsif WE = '0' and RE = '1' then
                DATA_OUT <= DATA;
            else
                DATA_OUT <= "ZZZZZZZZ";
            end if;
        end if;
    end process;
	 -- enter your statements here --

end shift_rx;
