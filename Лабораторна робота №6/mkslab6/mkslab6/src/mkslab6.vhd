library ieee;
use ieee.std_logic_1164.all;

entity ROM is
  port ( 	 
  		 CEO: in std_logic;
  		 Addr : in std_logic_vector(3 downto 0);
         Dout : out std_logic_vector(3 downto 0) 
		);
end entity ROM;

architecture ROM of ROM is
  type memory is array (0 to 15) of std_logic_vector(3 downto 0);
  constant my_Rom : memory := (
    0  => "0000",
    1  => "0001",
    2  => "0010",
    3  => "0011",
    4  => "0100",
    5  => "0101",
    6  => "0110",
    7  => "0111",
    8  => "1000",
    9  => "1001",
    10 => "1010",
    11 => "1011",
    12 => "1100",
    13 => "1101",
    14 => "1110",
    15 => "1111");
begin
   process (Addr, CEO)
   begin
	 if CEO'event and CEO='1' then
	     case Addr is
	       when x"0" => Dout <= my_Rom(0);
	       when x"1" => Dout <= my_Rom(1);
	       when x"2" => Dout <= my_Rom(2);
	       when x"3" => Dout <= my_Rom(3);
	       when x"4" => Dout <= my_Rom(4);
	       when x"5" => Dout <= my_Rom(5);
	       when x"6" => Dout <= my_Rom(6);
	       when x"7" => Dout <= my_Rom(7);
	       when x"8" => Dout <= my_Rom(8);
	       when x"9" => Dout <= my_Rom(9);
	       when x"A" => Dout <= my_Rom(10);
	       when x"B" => Dout <= my_Rom(11);
	       when x"C" => Dout <= my_Rom(12);
	       when x"D" => Dout <= my_Rom(13);
	       when x"E" => Dout <= my_Rom(14);
	       when x"F" => Dout <= my_Rom(15);
	       when others => Dout <= "0000";
		end case;
	 end if;
  end process;
end architecture ROM;

