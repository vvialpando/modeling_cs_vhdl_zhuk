-------------------------------------------------------------------------------
--
-- Title       : mkslab8
-- Design      : lab8
-- Author      : MG
-- Company     : 1
--
-------------------------------------------------------------------------------
--
-- File        : mkslab8.vhd
-- Generated   : Sun May 13 21:00:48 2018
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {mkslab8} architecture {mkslab8}}

library 
IEEE;  
use IEEE.STD_LOGIC_1164.all;  
use IEEE.STD_LOGIC_UNSIGNED.all;  
entity Reg is  generic(n: 
integer:=31);  
   port(  
      CLK : in STD_LOGIC;  
      WE : in STD_LOGIC;  
X : in STD_LOGIC_VECTOR(n downto 0);  
Y : out STD_LOGIC_VECTOR(n downto 0)  
      );  
end Reg;  
  
  
architecture Reg of Reg is begin  
   process (CLK)       variable DATA: 
STD_LOGIC_VECTOR(n downto 0);               begin  
      if CLK'event and CLK = '1' then  
         if WE = '1' then     
            DATA := X;     
         elsif WE = '0'  then  
            Y <= DATA;  
       else  
            Y <=  
"ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";  
   end if;   end if;  
   end process;  
end Reg;  
  

------------------------------------------------------------------------------- --  
-- Title       : DeMux  
-- Design      : lab  
-- Author      : Volodymyr Hanas  
-- Company     : LPNU  
--  
------------------------------------------------------------------------------- --  
-- File        : C:\My_Designs\lab83\lab\src\DeMux.vhd  --  
------------------------------------------------------------------------------- --  
-- Description :   
--  
-------------------------------------------------------------------------------  
  
--{{ Section below this comment is automatically maintained  
--   and may be overwritten  
--{entity {DeMux} architecture {DeMux}}  
  
library IEEE; use 
IEEE.STD_LOGIC_1164.all;  
  
entity DeMux is  
    port(  
       CLK : in STD_LOGIC;  
       Y : in STD_LOGIC_VECTOR(31 downto 0);  
       Y7 : out STD_LOGIC_VECTOR(3 downto 0);  
       Y6 : out STD_LOGIC_VECTOR(3 downto 0);  
       Y5 : out STD_LOGIC_VECTOR(3 downto 0);  
       Y4 : out STD_LOGIC_VECTOR(3 downto 0);  
       Y3 : out STD_LOGIC_VECTOR(3 downto 0);  
       Y2 : out STD_LOGIC_VECTOR(3 downto 0);  
       Y1 : out STD_LOGIC_VECTOR(3 downto 0);  
       Y0 : out STD_LOGIC_VECTOR(3 downto 0)  
        );  
end DeMux;  
  
--}} End of automatically maintained section  
  
architecture DeMux of DeMux is begin     
      Y7(3)<=   Y(31);  
      Y7(2)<=   Y(30);  
      Y7(1)<=   Y(29);  
      Y7(0)<=   Y(28);  
        
      Y6(3)<=   Y(27);  
      Y6(2)<=   Y(26);  
      Y6(1)<=   Y(25);  
      Y6(0)<=   Y(24);    
        
      Y5(3)<=   Y(23);        Y5(2)<=   Y(22);  
      Y5(1)<=   Y(21);  
      Y5(0)<=   Y(20);  
        
      Y4(3)<=   Y(19);  
      Y4(2)<=   Y(18);  
      Y4(1)<=   Y(17);  
      Y4(0)<=   Y(16);  
        
      Y3(3)<=   Y(15);  
      Y3(2)<=   Y(14);  
      Y3(1)<=   Y(13);  
      Y3(0)<=   Y(12);  
        
      Y2(3)<=   Y(11);  
      Y2(2)<=   Y(10);  
      Y2(1)<=   Y(9);  
      Y2(0)<=   Y(8);  
        
      Y1(3)<=   Y(7);  
      Y1(2)<=   Y(6);  
      Y1(1)<=   Y(5);  
      Y1(0)<=   Y(4);  
        
      Y0(3)<=   Y(3);  
      Y0(2)<=   Y(2);  
      Y0(1)<=   Y(1);  
      Y0(0)<=   Y(0);  
        
        
                          
end DeMux;  
library 
IEEE;  
use IEEE.STD_LOGIC_1164.all;      
use IEEE.STD_LOGIC_UNSIGNED.all;     
  
entity LCDConv7 is  
    port(  
    Y7 : in STD_LOGIC_VECTOR(3 downto 0)   ;  
    LCD7 : out STD_LOGIC_VECTOR(6 downto 0)          ); end 
LCDConv7;  
  
architecture LCDConv7 of LCDConv7 is           
begin  
  
   process (Y7)  
   variable n: integer;     
     
   begin       n := 
Conv_Integer(Y7);       case 
n is          when 
0=>LCD7<="1110111";     
    when 
1=>LCD7<="0100100";     
    when 
2=>LCD7<="1000101";     
    when 
3=>LCD7<="1101101";     
    when 
4=>LCD7<="0101110";     
    when 
5=>LCD7<="1101011";     
    when 
6=>LCD7<="1111011";     
    when 
7=>LCD7<="0100101";     
    when 
8=>LCD7<="1111111";      
    when 
9=>LCD7<="1101111";     
    when others 
=>LCD7<="0000000";  
      end case;                   
   end process;  
end LCDConv7; 